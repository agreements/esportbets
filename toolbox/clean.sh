#!/bin/sh -e
## ┌───────────────────────────────────────────────────────────────────────────┐
## │ Description:  Recursively clean directories                               │
## │ Author:       Mathieu Marques                                             │
## │ Last update:  2013.10.07                                                  │
## │ Usage:        ./clean.sh [directory]                                      │
## └───────────────────────────────────────────────────────────────────────────┘


clean()
{
    find $1 -name "*~"               -exec rm '{}' \; -print
    find $1 -name ".*~"              -exec rm '{}' \; -print
    find $1 -name "#*#"              -exec rm '{}' \; -print
    find $1 -name "*.o"              -exec rm '{}' \; -print
    find $1 -name "*.pyc"            -exec rm '{}' \; -print
    find $1 -name ".goutputstream-*" -exec rm '{}' \; -print
}

if [ $1 ]
then
    if [ -d $1 ]
    then
	clean $1
    else
	echo "$1 is not a directory"
    fi
else
    clean `pwd`
fi
