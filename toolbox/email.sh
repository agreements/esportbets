#!/bin/sh -e
## ┌───────────────────────────────────────────────────────────────────────────┐
## │ Description:  Launch a local Python email server                          │
## │ Author:       Mathieu Marques                                             │
## │ Last update:  2013.07.07                                                  │
## │ Usage:        ./email.sh                                                  │
## └───────────────────────────────────────────────────────────────────────────┘


python -m smtpd -n -c DebuggingServer localhost:1025