#!/bin/sh -e
## ┌───────────────────────────────────────────────────────────────────────────┐
## │ Description:  Convert JPG files to PNG using command line tool "convert"  │
## │ Author:       Mathieu Marques                                             │
## │ Last update:  2013.07.07                                                  │
## │ Usage:        ./convert.sh                                                │
## └───────────────────────────────────────────────────────────────────────────┘


for OLD in *
do
    NEW=`echo $OLD | tr '_' '-' | tr [A-Z] [a-z]`
    mv -v $OLD $NEW
    JPG=`echo $NEW | tr "png" "jpg"`
    convert $NEW $JPG
done