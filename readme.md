# First things first

This very guideline will focus on setting up the project on a standard Ubuntu installation. You will need the command line tool `pip` to install most of requirements for Esport Bets.

> *Only then you have my permission to clone the sources.*

# Dependencies

The project requires Python 2.7.x.

### 1.1 General requirements

```text
apt-get install python-pip          # Python Package Index
pip install django==1.5.5           # Django 1.5.5
pip install pytz                    # Support for time zones
pip install django-userena          # Userena: third-party application
pip install python-mimeparse        # Tastypie: dependency
pip install python-dateutil         # Tastypie: dependency
pip install lxml defusedxml         # Tastypie: optionnal (XML serializer)
pip install pyyaml                  # Tastypie: optionnal (YAML serializer)
pip install biplist                 # Tastypie: optionnal (PList serializer)
pip install django-tastypie         # Tastypie: third-party application
```

### 1.2 Installing IPython (optionnal)

Install IPython to have a more optimized shell within Django.

```text
pip install ipython
```

### 1.3 Setting up a PostgreSQL Database (optionnal)

Install PostgreSQL requirements in order to use a PostgreSQL database.

```text
apt-get install libpq-dev
apt-get install python-dev
apt-get install postgresql postgresql-contrib
pip install psycopg2
sudo su - postgres
```

Then create a database, a user and bind the user to the database with corresponding rights.

```text
createdb name
createuser -P username
psql
```

Grant permissions to the newly create user on your database.

```sql
GRANT ALL PRIVILEGES ON DATABASE name TO username
```

# Get the sources

First, install Git. Then clone the source from Bitbucket.

```text
apt-get install git
git clone https://angrybacon@bitbucket.org/angrybacon/esportbets.git
```

# Launch the server

Esport Bets is a standard Django application. You can launch the development server on you own machine to test stuff.

```text
cd esportbets/sources/
python manage.py runserver [address:][port]
```

Then watch it on your browser. The default address is `127.0.0.1`, the default port is `8000`.

# What now ?

There could be remaining bugs that aren't already filed, be sure to report them if you find any.