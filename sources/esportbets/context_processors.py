#! -*- coding: utf-8 -*-
from datetime import datetime


# ──────────────────────────────────────────────────────────────────────────────
def date(request):
    '''Make the template variable ``now`` available in all templates

    :param request: the Django request object
    '''

    now = datetime.now()
    return { 'now': now }
