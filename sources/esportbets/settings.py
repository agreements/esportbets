from django.utils.translation import ugettext_lazy as _
from os import path


# Project path for further access
PROJECT_PATH = path.realpath(path.dirname(__file__))

# Debug settings
DEBUG          = True
TEMPLATE_DEBUG = DEBUG

# Admin settings
ADMINS         = (
    ('Mathieu Marques', 'mathieumarques78@gmail.com'),
)
MANAGERS = ADMINS

# Database settings
DB_POSTGRESQL = {
    'ENGINE':   'django.db.backends.postgresql_psycopg2',
    'NAME':     'esportbets',
    'USER':     'mathieu',
    'PASSWORD': 'tumbleweed',
    'HOST':     'localhost',
}
DB_SQLITE = {
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME':   'esportbets/default.sqlite',
}
DB_ALWAYSDATA = {
    'ENGINE':   'django.db.backends.postgresql_psycopg2',
    'NAME':     'angrybacon_esportbets',
    'USER':     'angrybacon',
    'PASSWORD': '',
    'HOST':     'postgresql1.alwaysdata.com',
}
DATABASES = {
    'default': DB_SQLITE
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# ID of the site
SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Ignore RuntimeWarning for naive datetime objects
import warnings
import exceptions
warnings.filterwarnings('ignore', category=exceptions.RuntimeWarning, module='django.db.backends.sqlite3.base', lineno=53)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    PROJECT_PATH+'/static/',
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '1co_9^y)83$mvpe&w61re(a(m^c#=xkvm5@fc_hge-ezx8a04!'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

# Middleware list
MIDDLEWARE_CLASSES = (
    # Django middlewares
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    # Homemade middlewares
    'esportbets.middlewares.StatsMiddleware',
)

# URLs of the project
ROOT_URLCONF = 'esportbets.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'esportbets.wsgi.application'

# Template directories
TEMPLATE_DIRS = (
    PROJECT_PATH+'/templates/',
)

# Installed Applications
INSTALLED_APPS = (

    # Django
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.staticfiles',

    # Homemade
    'accounts',
    'dota',
    'helpers',
    'home',

    # Userena
    'userena',
    'easy_thumbnails',
    'guardian',
)

# Processors for generated templates basically gives them {{ request }}
TEMPLATE_CONTEXT_PROCESSORS = (
    # Django context processors
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',

    # Homemade context processors
    'esportbets.context_processors.date',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# Server side includes settings
ALLOWED_INCLUDE_ROOTS = (
    PROJECT_PATH+'/static/js/Homemade/',
)

# URLs settings
LOGIN_URL                   = '/accounts/signin/'
LOGOUT_URL                  = '/accounts/signout/'

# Email settings
EMAIL_BACKEND       = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS       = True
EMAIL_PORT          = 587
EMAIL_HOST          = 'smtp.gmail.com'
EMAIL_HOST_USER     = 'contact.esportbets@gmail.com'
EMAIL_HOST_PASSWORD = 'tumbleweed'

# Authentication backends
AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# Internationalization and localization settings
LANGUAGES = (
    ('en', _('English')),
    ('fr', _('French')),
)
LOCALE_PATHS = (
    path.dirname(path.dirname(PROJECT_PATH))+'/locale/',
)

# Userena general
ANONYMOUS_USER_ID          = -1
AUTH_PROFILE_MODULE        = 'accounts.Profile'
USERENA_USE_HTTPS          = False
USERENA_USE_MESSAGES       = True
USERENA_LANGUAGE_FIELD     = 'language'
USERENA_WITHOUT_USERNAMES  = False
USERENA_HIDE_EMAIL         = False
USERENA_HTML_EMAIL         = False
USERENA_USE_PLAIN_TEMPLATE = True

# Userena signup
USERENA_ACTIVATION_DAYS        = 7
USERENA_SIGNIN_AFTER_SIGNUP    = False
USERENA_SIGNIN_REDIRECT_URL    = '/accounts/%(username)s/'
USERENA_ACTIVATION_REQUIRED    = True
USERENA_ACTIVATION_DAYS        = 7
USERENA_ACTIVATION_NOTIFY      = True
USERENA_ACTIVATION_NOTIFY_DAYS = 2
USERENA_ACTIVATED              = 'ALREADY_ACTIVATED'
USERENA_REMEMBER_ME_DAYS       = ('a month', 30)
USERENA_FORBIDDEN_USERNAMES    = ('activate', 'me', 'password', 'signin', 'signout', 'signup')
USERENA_DISABLE_SIGNUP         = False

# Userena mugshot
USERENA_MUGSHOT_GRAVATAR        = True
USERENA_MUGSHOT_GRAVATAR_SECURE = USERENA_USE_HTTPS
USERENA_MUGSHOT_DEFAULT         = 'identicon'
USERENA_MUGSHOT_SIZE            = 80
USERENA_MUGSHOT_PATH            = 'mugshots/%(username)s/'

# Userena profiles
USERENA_DEFAULT_PRIVACY      = 'open'
USERENA_DISABLE_PROFILE_LIST = False
