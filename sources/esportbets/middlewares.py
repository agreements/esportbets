#! -*- coding: utf-8 -*-
from django.db import connection
from time import time
from operator import add
import re


# Courtesy of Hieu Nguyen, see http://stackoverflow.com/a/17777539/1824222
# In your base template, put this:
# <!-- STATS: %(time_total).2fs %(time_py).2fs %(time_db).2fs %(db_queries)d ENDSTATS -->


# ──────────────────────────────────────────────────────────────────────────────
class StatsMiddleware(object):

    # ──────────────────────────────────────
    def process_view(self, request, view_func, view_args, view_kwargs):
        '''
        '''

        # Get number of database queries before we do anything
        n = len(connection.queries)

        # Time the view
        start = time()
        response = view_func(request, *view_args, **view_kwargs)
        time_total = time() - start

        # Compute the database time for the queries just run
        db_queries = len(connection.queries) - n
        if db_queries:
            time_db = reduce(add, [float(q['time']) for q in connection.queries[n:]])
        else:
            time_db = 0.0

        # And backout Time_Py time
        time_py = time_total - time_db

        # Stats ready
        stats = {
            'tt': time_total,
            'py': time_py,
            'db': time_db,
            'nb': db_queries,
        }

        # Replace the comment if found
        if response:

            # Detects TemplateResponse which are not yet rendered
            try:
                if response.is_rendered:
                    rendered_content = response.content
                else:
                    rendered_content = response.rendered_content
            except AttributeError:
                rendered_content = response.content

            # Insert stats
            if rendered_content:
                s = rendered_content
                regexp = re.compile(
                    r'(?P<cmt><!--\s*STATS:(?P<fmt>.*?)ENDSTATS\s*-->)'
                )
                match = regexp.search(s)
                if match:
                    s = (s[:match.start('cmt')] +
                         match.group('fmt') % stats +
                         s[match.end('cmt'):])
                    response.content = s

        return response
