// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain code for the fantasy bets feature                      2014.07.24 │
// └───────────────────────────────────────────────────────────────────────────┘


// Buy a player
// ─────────────────────────────────────────────────────────────────────────────
function BuyPlayer(nID, nPosition, sUsername, sPrice) {

    // Checking if the user's team already contain this position
    var oRoom = $('#position-'+nPosition);
    if ( oRoom.hasClass('empty') ) {

        // Updating backend
        var oDatas = { 'csrfmiddlewaretoken': $.cookie('csrftoken') };
        $.post('/ajax/dota/players/buy/'+nID+'/', oDatas)
            .done(function(oResponse) {

                // Updating frontend (a better way is to simply move it)
                noty(oNotyMessages[oResponse['status']]);
                oRoom
                    .removeClass('empty')
                    .html('<a class="player" onclick="SellPlayer('+nID+', '+nPosition+', \''+sUsername+'\', \''+sPrice+'\');"><span class="text">'+sUsername+'</span><span class="price">'+sPrice+'</span></a>');
                $('#player-'+nID)
                    .addClass('empty')
                    .html('');
                UpdateCredits(oResponse['data']['credits']);
                UpdatePlayerCount();
            })
            .fail(function() {
                noty(oNotyError);
            });
    }

    // It does
    else
        noty(oNotyMessages['FANTASY_INVALID']);
}


// Sell a player
// ─────────────────────────────────────────────────────────────────────────────
function SellPlayer(nID, nPosition, sUsername, sPrice) {

    // Updating backend
    var oDatas = { 'csrfmiddlewaretoken': $.cookie('csrftoken') };
    $.post('/ajax/dota/players/sell/'+nID+'/', oDatas)
        .done(function(oResponse) {

            // Updating frontend (a better way is to simply move it)
            noty(oNotyMessages[oResponse['status']]);
            $('#position-'+nPosition)
                .addClass('empty')
                .html('');
            $('#player-'+nID)
                .removeClass('empty')
                .html('<a class="player" onclick="BuyPlayer('+nID+', '+nPosition+', \''+sUsername+'\', \''+sPrice+'\');"><span class="text">'+sUsername+'</span><span class="price">'+sPrice+'</span></a>');
            UpdateCredits(oResponse['data']['credits']);
            UpdatePlayerCount();
        })
        .fail(function() {
            noty(oNotyError);
        });
}


// Update the user's credits amount
// ─────────────────────────────────────────────────────────────────────────────
function UpdateCredits(nCredits) {

    // In case someone's tricking us
    if ( typeof(nCredits) === 'undefined' ) {
        var nCredits = -1;
    }

    // Adding commas by hand
    var aParts = nCredits.toFixed(2).split('.');
    var nL = aParts[0].length;
    var sCredits = '';
    for ( var i=nL-1; i>-1; --i ) {
        sCredits = ( i ? ( (nL-i)%3===0 ? ',' : '' ) : '' ) + aParts[0][i] + sCredits;
    }

    // Rendering the credits amount
    $('#credits-amount').html(sCredits+'.'+aParts[1]);
}


// Update the player count for the user's fantasy team
// ─────────────────────────────────────────────────────────────────────────────
function UpdatePlayerCount(nCount) {
    if ( typeof(nCount) === 'undefined' ) {
        var nCount = 5 - $('#players .empty').length;
    }
    $('#players-count').html(nCount);
}


// Show/hide the pool of players depending the size of the fantasy team
// ─────────────────────────────────────────────────────────────────────────────
function InitializePlayerPool() {

    // Initialize the togglers
    $('.ninja-toggler').click(function() {
        $(this).parent().next().slideToggle(300);
    });

    // Hide the player pool if team is full
    if ( $('#players .empty').length === 0 ) {
        $('#pool').hide();
    }
}
