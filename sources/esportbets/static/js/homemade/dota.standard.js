// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain code for the standard bets feature                     2014.07.24 │
// └───────────────────────────────────────────────────────────────────────────┘


// Global variables for further access
var aEnemies = {
    'dire':    'radiant',
    'radiant': 'dire'
};
var oBets = {};


// Submit the bets into the database
// ─────────────────────────────────────────────────────────────────────────────
function SubmitBets() {
    var oDatas = { 'csrfmiddlewaretoken': $.cookie('csrftoken') };
    oDatas['bets'] = {};
    for ( id in oBets ) {
        oDatas['bets'][id] = oBets[id];
    }
    $.post('/ajax/dota/standard/submit/', oDatas)
        .done(function(oResponse) {
            noty(oNotyMessages[oResponse['status']]);
        })
        .fail(function() {
            noty(oNotyError);
        });
}


// Invoke a dialog box to notify the user of what he is doing
// ─────────────────────────────────────────────────────────────────────────────
function ConfirmBets() {

    // Is there anything to submit
    var nBets = $('#encounters .encounter.valid').length;
    if ( oBets && nBets > 0 ) {

        // Building the heads up
        var sDialog = '';
        var sResult = {
            'dire':    [ 'victory', 'defeat' ],
            'radiant': [ 'defeat', 'victory' ]
        };
        var sDire    = '';
        var sRadiant = ''
        for ( id in oBets ) {
            sBet = oBets[id];
            if ( sBet.length > 0 ) {
                oBet = $('#encounter-'+id);
                sDire = oBet.find('.faction.dire .team').eq(0).html();
                sRadiant = oBet.find('.faction.radiant .team').eq(0).html();
                sDialog += '<tr>' +
                    '<td><span class="' + sResult[sBet][0] + '">' + sRadiant + '</span></td>' +
                    '<td><span class="' + sResult[sBet][1] + '">' + sDire + '</span></td>' +
                    '</tr>';
            }
        }
        sDialog = '<p>' +
            'You are going to bet for the following encounter' +
            (nBets > 1 ? 's' : '') + ':<br /><br />' +
            '<table class="table">'+sDialog+'</table>' +
            '</p>';

        // Rendering the dialog
        $('#dialog').html(sDialog).dialog('open');
    }
}


// Enable or disable the submit button
// ─────────────────────────────────────────────────────────────────────────────
function ToggleSubmitButton() {
    switch ( $('#encounters .encounter.valid').length) {
    case 0:
        $('#submit-bets').button('option', 'disabled', true);
        break;
    default:
        $('#submit-bets').button('option', 'disabled', false);
    }
}


// Mark the encounters depending the user's choice
// ─────────────────────────────────────────────────────────────────────────────
function ToggleBets(oEncounter, sBet) {
    var oWinner = oEncounter.find('.faction.'+sBet+' .team').eq(0);
    var oLoser = oEncounter.find('.faction.'+aEnemies[sBet]+' .team').eq(0)
    if ( oWinner.hasClass('victory') && oLoser.hasClass('defeat') ) {
        oWinner.removeClass('victory defeat');
        oLoser.removeClass('victory defeat');
        oEncounter.removeClass('valid');
        oBets[oEncounter.data('pk')] = '';
    }
    else {
        oWinner.removeClass('defeat').addClass('victory');
        oLoser.removeClass('victory').addClass('defeat');
        oEncounter.addClass('valid');
        oBets[oEncounter.data('pk')] = sBet;
    }
}


// Initialize the standard betting system
// ─────────────────────────────────────────────────────────────────────────────
function InitializeStandard() {

    // Initialize the interactive rows containing upcoming encounters
    $('#encounters tr.interactive, #bets tr.interactive')
        .click(function() { $(this).find('.ninja').slideToggle(200); })
        .on('selectstart', function() { return false; });

    // Initialize the bet togglers
    $('#encounters .bet-toggler').click(function(oEvent) {
        var oEncounter = $(this).parents('.encounter').eq(0);
        var sBet = $(this).parents('.faction').eq(0).data('faction');
        ToggleBets(oEncounter, sBet);
        ToggleSubmitButton();
        oEvent.stopPropagation();
    });

    // Initialize the confirmation dialog
    var oDialog = $('#dialog');
    oDialog.dialog({
        'title': 'Please confirm your bets',
        'buttons': {
            'confirm': function() {
                SubmitBets();
                $(this).dialog('close');
            },
            'cancel': function() {
                $(this).dialog('close');
            }
        }
    });
}
