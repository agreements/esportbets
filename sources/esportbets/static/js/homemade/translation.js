// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain code for the language selector                         2014.06.29 │
// └───────────────────────────────────────────────────────────────────────────┘


// Initialize the language selector
// ─────────────────────────────────────────────────────────────────────────────
function InitializeTranslation() {
    $('#language-selector').change(function() {
        $(this).parent().submit();
    });
}
