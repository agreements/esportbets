// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain code altering the behavior of AJAX requests            2014.05.23 │
// └───────────────────────────────────────────────────────────────────────────┘


// Initialize behavior for AJAX requests
// ─────────────────────────────────────────────────────────────────────────────
function InitializeAJAX() {
    $(document).ajaxStart(function() {
        $('html').addClass('busy');
    });
    $(document).ajaxStop(function() {
        $('html').removeClass('busy');
    });
}
