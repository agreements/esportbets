// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain code for the menu                                      2014.06.29 │
// └───────────────────────────────────────────────────────────────────────────┘


// Add an icon besides external links
// ─────────────────────────────────────────────────────────────────────────────
function InitializeExternalLinks() {
    $('a.external')
        .attr('target', '_blank')
        .after('<span class="ui-icon ui-icon-extlink" style="display:inline-block;position:relative;top:2px;"></span>');
}


// jQuerize buttons
// ─────────────────────────────────────────────────────────────────────────────
function InitializeButtons() {
    $('button.jquerized, input.jquerized')
        .button()
        .bind('mouseup', function() { $(this).blur(); });
}
