// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain the default behavior of the jQuery dialogs             2014.05.16 │
// └───────────────────────────────────────────────────────────────────────────┘


// Default options
function InitializeDialogs() {
    var oTransition = {
        'effect':  'clip',
        'duration': 200,
        'easing':   'easeOutCirc'
    };

    $.extend($.ui.dialog.prototype.options, {
        'autoOpen':    false,
        'modal':       true,
        'resizable':   false,
        'width':       350,
        'show': oTransition,
        'hide': oTransition,
        'open': function() {
            $('body').css('overflow', 'hidden');
            $('.ui-widget-overlay').css('top', $(document).scrollTop().toString()+'px');
            $('.ui-dialog-titlebar-close').hide();
        },
        'beforeClose': function() {
            $('body').css('overflow', 'inherit');
        }
    });
}
