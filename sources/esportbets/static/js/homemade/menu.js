// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain code for the menu                                      2014.06.29 │
// └───────────────────────────────────────────────────────────────────────────┘


// Initialize the sidebar with hover effects
// ─────────────────────────────────────────────────────────────────────────────
function InitializeMenu() {

    // First level
    $('#wrapper-menu').hover(function() {
        $(this).find('.menu-item.last .sprite').fadeToggle(0);
        var oContent = $(this).find('.content');
        if ( $(this).hasClass('opened') ) {
            oContent.fadeOut(0);
            $(this).removeClass('opened', 50);
        }
        else {
            $(this).addClass('opened', 50, function() {
                oContent.fadeIn(0);
            });
        }
    });

    // Second level
    $('#menu-items .content.expendable').click(function() {
        $(this).find('.submenu').eq(0).slideToggle(100)
        var oIcon = $(this).find('.ui-icon').eq(0);
        if ( oIcon.hasClass('ui-icon-carat-1-e') ) {
            oIcon.switchClass('ui-icon-carat-1-e', 'ui-icon-carat-1-s', 0);
        }
        else {
            oIcon.switchClass('ui-icon-carat-1-s', 'ui-icon-carat-1-e', 0);
        }
    });

    // Disable the expanding child click events
    $('#menu-items .expendable .submenu li').click(function(oEvent) {
        oEvent.stopPropagation();
    });

    // Disable selection on menu items
    $('#menu .entry, #menu .submenu li').on('selectstart',function() { return false; });
}
