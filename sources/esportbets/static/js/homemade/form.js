// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain code for the forms                                     2013.07.09 │
// └───────────────────────────────────────────────────────────────────────────┘


// Form submission settings
var nDelay   = 2000;
var bLoading = false;
var oTimeOut = false;


// Compute a given input field and delay the form submission
// ─────────────────────────────────────────────────────────────────────────────
function ComputeFields(oForm) {
    if ( !bLoading ) {
        oTimeOut = setTimeout(function() {
            bLoading = true;
            oForm.submit();
            bLoading = false;
        }, nDelay);
    }
}


// Add AJAX handlers for validating and submitting
// ─────────────────────────────────────────────────────────────────────────────
function InitializeForms(aForms) {
    var sFormLabel = '';
    $(aForms.join(',')).each(function() {

        // AJAX submission
        sFormLabel = $(this).attr('id').split('-')[1];
        $(this).submit(function() {
            $.ajax({
                'data':    $(this).serialize(),
                'type':    $(this).attr('method'),
                'url':     '/ajax/forms/submit/'+sFormLabel+'/',
                'success': function(response) { noty(oNotyMessages[response['status']]); }
            });
            return false;  // So it does not reload the page
        });

        // Events for when to submit the form
        var oForm = $(this);
        $(this).find(':text').each(function() {
            $(this).keyup(function() {
                if ( oTimeOut )
                    clearTimeout(oTimeOut);
                ComputeFields(oForm);
            });
        });

    });
}
