// ┌───────────────────────────────────────────────────────────────────────────┐
// │ Contain code for Noty initialization                           2013.07.14 │
// └───────────────────────────────────────────────────────────────────────────┘


// Notifications messages
// ─────────────────────────────────────────────────────────────────────────────
var oNotyError = {
    'type': 'error',
    'text': '<b>An error has been encountered !</b> We have been notified'
};

var oNotySuccess = {
    'type': 'success',
    'text': '<b>Heads up !</b> Your modifications have been successfully saved'
}

var oNotyMessages = {

    // Meta
    'ERROR':   oNotyError,
    'SUCCESS': oNotySuccess,

    // Forms
    'FORM_IS_NOT_VALID': {
        'type': 'warning',
        'text': '<b>Beware !</b> The form contains error'
    },

    // Fantasy
    'FANTASY_INVALID': {
        'type': 'warning',
        'text': '<b>Beware !</b> You already have a player in this position'
    },
    'FANTASY_PLAYER_UNKNOWN': {
        'type': 'alert',
        'text': '<b>Beware !</b> Come on wizard, stop trying shit'
    },

    // Wallet
    'WALLET_EMPTY': {
        'type': 'warning',
        'text': '<b>Beware !</b> You need more credits in order to buy this player'
    }
}


// Initialize Noty with custom default settings
// ─────────────────────────────────────────────────────────────────────────────
function InitializeNotifications() {
    jQuery.noty.defaults = {
        'layout':               'topRight',
        'theme':                'customTheme',
        'type':                 'alert',    // Or 'alert', 'confirm', 'error', 'information', 'success', 'warning'
        'text':                 '',
        'dismissQueue':         true,       // Queue feature
        'template':             '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        'animation': {
            'open':             { 'height':'toggle' },
            'close':            { 'height':'toggle' },
            'easing':           'swing',
            'speed':            200         // Opening and closing animation speed
        },
        'timeout':              4000,       // Set false for sticky notifications
        'force':                false,      // Adds notification to the beginning of queue when set to true
        'modal':                false,
        'closeWith':            ['click'],  // Or ['click', 'button', 'hover']
        'callback': {
            'onShow':           function() {},
            'afterShow':        function() {},
            'onClose':          function() {},
            'afterClose':       function() {}
        },
        'buttons':              false       // An array of buttons
    };
}
