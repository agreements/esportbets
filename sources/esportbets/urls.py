from django.conf.urls import patterns, include, url
from django.contrib import admin
from tastypie.api import Api

from accounts.api import UserResource
from dota.api import *


admin.autodiscover()

# Django URLs
urlpatterns = patterns('',
    url(r'^admin/',     include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^i18n/',      include('django.conf.urls.i18n')),
)

# Homemade URLs
urlpatterns += patterns('',
    url(r'^accounts/', include('accounts.urls')),
    url(r'^ajax/',     include('ajax.urls')),
    url(r'^dota/',     include('dota.urls')),
    url(r'^home/',     include('home.urls')),
)

# Third party URLs
API = {
    'dota':      Api(api_name='1'),
    'standard':  Api(api_name='0'),
    'starcraft': Api(api_name='2'),
}
API['dota'].register(BetResource())
API['dota'].register(EncounterResource())
API['dota'].register(FantasyResource())
API['dota'].register(HeroResource())
API['dota'].register(LeagueResource())
API['dota'].register(MatchResource())
API['dota'].register(PlayerResource())
API['dota'].register(TeamResource())
API['standard'].register(UserResource())
urlpatterns += patterns('',
    url(r'^api/',      include(API['dota'].urls)),
    url(r'^api/',      include(API['standard'].urls)),
    url(r'^api/',      include(API['starcraft'].urls)),
    url(r'^accounts/', include('userena.urls')),
)

# Flatpage URLs
urlpatterns += patterns('django.contrib.flatpages.views',
    url(r'^about/$', 'flatpage', { 'url':'/about/' }, name='about'),
)
