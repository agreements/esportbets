#! -*- coding: utf-8 -*-


RESPONSES = {

    # Forms
    'FORM_404':               { 'status':'ERROR',                  'message':'Model object not found' },
    'FORM_CRITICAL':          { 'status':'ERROR',                  'message':'Model and/or form missing during initialization' },
    'FORM_IS_NOT_VALID':      { 'status':'FORM_IS_NOT_VALID',      'message':'Form is not valid' },
    'FORM_SUBMITTED':         { 'status':'SUCCESS',                'message':'Form is submitted' },

    # Fantasy
    'FANTASY_INVALID':        { 'status':'FANTASY_INVALID',        'message':'Position is already taken' },
    'FANTASY_PLAYER_404':     { 'status':'ERROR',                  'message':'Player not found' },
    'FANTASY_PLAYER_BOUGHT':  { 'status':'SUCCESS',                'message':'Fantasy player bought' },
    'FANTASY_PLAYER_SOLD':    { 'status':'SUCCESS',                'message':'Fantay player sold' },
    'FANTASY_PLAYER_UNKNOWN': { 'status':'FANTASY_PLAYER_UNKNOWN', 'message':'Player not found in team' },
    'FANTASY_TEAM_404':       { 'status':'ERROR',                  'message':'Fantasy team not found' },

    # Standard
    'STANDARD_SUBMITTED':     { 'status':'SUCCESS',                'message':'Standard bets submitted' },

    # Wallet
    'WALLET_EMPTY':           { 'status':'WALLET_EMPTY',           'message':'Not enough credit' },
}
