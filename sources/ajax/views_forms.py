#! -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.decorators.http import require_POST
from json import dumps

from accounts.models import Profile
from ajax.responses import RESPONSES
from dota.forms import FantasyForm
from dota.models import Fantasy
from helpers.decorators.requireajax import require_AJAX
from userena.forms import EditProfileForm


OBJECTS = {
    'fantasy': { 'model':Fantasy, 'form':FantasyForm },
    'profile': { 'model':Profile, 'form':EditProfileForm },
}


# ──────────────────────────────────────────────────────────────────────────────
@login_required
@require_AJAX
def submit(request, label):
    """Submit a form using AJAX using POST datas

    :param request: the Django request object
    :param label: the form label
    :returns: <class 'HttpResponse'>
    """

    # Fetching model and form objects
    try:
        model = OBJECTS.get(label)['model']
        form = OBJECTS.get(label)['form']
    except TypeError:
        model = form = None
        response = dumps(RESPONSES['FORM_CRITICAL'])

    # Submitting the form
    if model and form:
        try:
            model = model.objects.get(user=request.user.id)
        except model.DoesNotExist:
            model = None
            response = dumps(RESPONSES['FORM_404'])
        form = form(request.POST, instance=model)
        if form.is_valid():
            model = form.save(commit=False)
            model.user = request.user  # This field ain't in the form
            form.save()
            response = dumps(RESPONSES['FORM_SUBMITTED'])
        else:
            response = dumps(RESPONSES['FORM_IS_NOT_VALID'])

    return HttpResponse(response, content_type='application/json')
