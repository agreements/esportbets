#! -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.decorators.http import require_POST
from json import dumps


from ajax.responses import RESPONSES
from dota.models import Bet, Encounter
from helpers.decorators.requireajax import require_AJAX


# ──────────────────────────────────────────────────────────────────────────────
@login_required
@require_AJAX
@require_POST
def submit(request):
    """Submit a bet from upcoming encounters

    :param request: the Django request object
    :returns: <class 'HttpResponse'>
    """

    user = request.user
    datas = dict(request.POST.iteritems())
    datas.pop('csrfmiddlewaretoken')
    for k,bet in datas.iteritems():
        if bet:
            pk = k.replace(']', '').split('[')[1]  # k is in the form of 'bets[pk]'
            bet = bet[0].upper()                   # From 'dire' to 'D'
            try:
                encounter = Encounter.objects.get(pk=pk)
            except Encounter.DoesNotExist:
                encounter = None
            if encounter:
                try:
                    bet = Bet.objects.get(user=user.pk, encounter=pk)
                except Bet.DoesNotExist:
                    bet = Bet(user=user, encounter=encounter, bet=bet).save()
    response = dumps(RESPONSES['STANDARD_SUBMITTED'])
    return HttpResponse(response, content_type='application/json')
