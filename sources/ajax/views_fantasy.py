#! -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseServerError
from json import dumps

from ajax.responses import RESPONSES
from dota.models import Fantasy, Player
from helpers.decorators.requireajax import require_AJAX


# ──────────────────────────────────────────────────────────────────────────────
@login_required
@require_AJAX
def buy(request, id):
    """Buy a Dota 2 player and add it to the user's team

    :param request: the Django request object
    :param id: the player's ID
    :returns: <class 'HttpResponse'>, <class 'HttpResponseServerError'>
    """

    # Fetching the user's profile
    try:
        profile = request.user.profile
    except Profile.DoesNotExist:
        return HttpResponseServerError()

    # Fetching the player
    try:
        player = Player.objects.get(id=int(id))
    except Player.DoesNotExist:
        player = None
        response = dumps(RESPONSES['FANTASY_PLAYER_404'])

    # Fetching the user's team
    try:
        fantasy = Fantasy.objects.get(user=request.user.id)
    except Fantasy.DoesNotExist:
        fantasy = None
        response = dumps(RESPONSES['FANTASY_TEAM_404'])

    # Buying the player
    if player and fantasy:

        # Was the choice valid ?
        if not player.position in [ p.position for p in fantasy.players.all() ]:

            # Does the user have enough money ?
            if player.price > profile.credits:
                response = dumps(RESPONSES['WALLET_EMPTY'])

            # He does
            else:

                # Taking the money
                profile.credits -= player.price
                profile.save()

                # Adding the player
                fantasy.players.add(player.id)
                response = RESPONSES['FANTASY_PLAYER_BOUGHT']
                response['data'] = { 'credits':profile.credits }
                response = dumps(response)

        # It was not
        else:
            response = dumps(RESPONSES['PLAYER_INVALID'])

    return HttpResponse(response, content_type='application/json')


# ──────────────────────────────────────────────────────────────────────────────
@login_required
@require_AJAX
def sell(request, id):
    """Sell a Dota 2 player and remove it from the user's team

    :param request: the Django request object
    :param id: the player's ID
    :returns: <class 'HttpResponse'>, <class 'HttpResponseServerError'>
    """

    # Fetching the user's profile
    try:
        profile = request.user.profile
    except Profile.DoesNotExist:
        return HttpResponseServerError()

    # Fetching the player
    try:
        player = Player.objects.get(id=int(id))
    except Player.DoesNotExist:
        player = None
        response = dumps(RESPONSES['FANTASY_PLAYER_404'])

    # Fetching the user's team
    try:
        fantasy = Fantasy.objects.get(user=request.user.id)
    except Fantasy.DoesNotExist:
        fantasy = None
        response = dumps(RESPONSES['FANTASY_TEAM_404'])

    # Selling the player
    if player and fantasy:

        # Does he even have the player ?
        if player in fantasy.players.all():

            # Giving the money
            profile.credits += player.price
            profile.save()

            # Removing the player
            fantasy.players.remove(player.id)
            response = RESPONSES['FANTASY_PLAYER_SOLD']
            response['data'] = { 'credits':profile.credits }
            response = dumps(response)


        # He does not
        else:
            response = dumps(RESPONSES['FANTASY_PLAYER_UNKNOWN'])

    return HttpResponse(response, content_type='application/json')
