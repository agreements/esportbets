from django.conf.urls import patterns, url

from dota.forms import FantasyForm


urlpatterns = patterns('ajax.views_forms',
    url(r'^forms/submit/(?P<label>[^/]+)/$',   'submit',   name='form-submit'),
)

urlpatterns += patterns('ajax.views_fantasy',
    url(r'^dota/players/buy/(?P<id>[0-9]+)/$',  'buy',  name='dota-fantasy-buy'),
    url(r'^dota/players/sell/(?P<id>[0-9]+)/$', 'sell', name='dota-fantasy-sell'),
)

urlpatterns += patterns('ajax.views_standard',
    url(r'^dota/standard/submit/$', 'submit', name='dota-standard-submit'),
)
