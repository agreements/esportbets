#! -*- coding: utf-8 -*-
from django.shortcuts import render


# ──────────────────────────────────────────────────────────────────────────────
def home(request):
    '''The homepage
    :param request: the Django request object
    :returns: <type 'HttpResponse'>
    '''

    # Rendering the page
    context = {}
    response = render(request, 'home/home.html', context)
    return response
