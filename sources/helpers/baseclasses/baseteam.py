#! -*- coding: utf-8 -*-
from django.db import models


# ──────────────────────────────────────────────────────────────────────────────
class BaseTeam(models.Model):
    """Defines a team
    """

    # ──────────────────────────────────────
    class Meta:
        abstract = True
        ordering = ['name']
