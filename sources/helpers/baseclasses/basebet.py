#! -*- coding: utf-8 -*-
from datetime import date
from django.contrib.auth.models import User
from django.db import models


# ──────────────────────────────────────────────────────────────────────────────
class BaseBet(models.Model):
    """Defines a bet
    """

    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)


    # ──────────────────────────────────────
    def __unicode__(self):
        return unicode('{}'.format(self.user))

    # ──────────────────────────────────────
    class Meta:
        abstract = True
