#! -*- coding: utf-8 -*-
from django.db import models

from basehuman import BaseHuman


# ──────────────────────────────────────────────────────────────────────────────
class BasePlayer(BaseHuman):
    """Defines a player
    """

    username = models.CharField(max_length=32, unique=True)
    price    = models.FloatField(default=1000.0)

    # ──────────────────────────────────────
    def __unicode__(self):
        return unicode(self.username)

    # ──────────────────────────────────────
    class Meta:
        abstract = True
        ordering = ['username']
