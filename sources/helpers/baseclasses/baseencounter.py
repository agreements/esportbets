#! -*- coding: utf-8 -*-
from datetime import date
from django.db import models


# ──────────────────────────────────────────────────────────────────────────────
class BaseEncounter(models.Model):
    """Defines an encounter
    """

    league = models.ForeignKey('League', on_delete=models.SET_NULL, blank=True, null=True)
    date   = models.DateTimeField(blank=True, null=True)


    # ──────────────────────────────────────
    def __unicode__(self):
        return unicode('{} VS {}'.format(self.dire, self.radiant))

    # ──────────────────────────────────────
    class Meta:
        abstract = True
