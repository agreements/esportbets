#! -*- coding: utf-8 -*-
from tastypie.authentication import BasicAuthentication
from tastypie.resources import ModelResource


# ──────────────────────────────────────────────────────────────────────────────
class BaseResource(ModelResource):
    """Defines a resource
    """

    # ──────────────────────────────────────
    class Meta:
        allowed_methods = [ 'get' ]
        authentication  = BasicAuthentication()
