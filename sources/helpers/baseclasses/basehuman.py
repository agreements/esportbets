#! -*- coding: utf-8 -*-
from datetime import date
from django.db import models


GENDERS = (
    ('F', 'Female'),
    ('M', 'Male'),
)


# ──────────────────────────────────────────────────────────────────────────────
class BaseHuman(models.Model):
    """Defines a human
    """

    full_name  = models.CharField(max_length=64, blank=True)
    gender     = models.CharField(max_length=1, choices=GENDERS, default=GENDERS[1][0], blank=True)
    birth_date = models.DateField(blank=True, null=True)

    # ──────────────────────────────────────
    def age(self):
        """Returns the human's age

        :returns: <type 'int'>
        """

        result = 0
        if self.birth_date:
            today = date.today()
            try:
                birthday = self.birth_date.replace(year=today.year)
            except ValueError:
                birthday = self.birth_date.replace(year=today.year, day=self.birth_date.day-1)
            if birthday > today:
                result = today.year - self.birth_date.year - 1
            else:
                result = today.year - self.birth_date.year
        return result

    # ──────────────────────────────────────
    def __unicode__(self):
        return unicode(self.full_name)

    # ──────────────────────────────────────
    class Meta:
        abstract = True
