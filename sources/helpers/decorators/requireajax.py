#! -*- coding: utf-8 -*-
from django.http import HttpResponseBadRequest


# ──────────────────────────────────────────────────────────────────────────────
def require_AJAX(function):
    """Return a bad request instance if the view is not using AJAX

    :param function: the view on which the decorator is applied to
    :returns: <type 'function'>, <type 'HttpResponseBadRequest'>
    """

    # ──────────────────────────────────────
    def wrap(request, *args, **kwargs):
        """Wrap an AJAX verification around the given request

        :param request: the AJAX request to check
        :returns: <type 'function'>, <type 'HttpResponseBadREquest'>
        """

        if not request.is_ajax():
            return HttpResponseBadRequest()
        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
