from django.template import Library


register = Library()

@register.filter(name='range')
def range(i):
    '''Return an iterable list for a static loop

    :param i: number of iterations
    '''

    return xrange(i)
