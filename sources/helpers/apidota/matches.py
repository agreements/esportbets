#! -*- coding: utf-8 -*-
from datetime import datetime as dt
from django.utils.timezone import utc

from api import APICall
from api import APICallError
from dota.models import League, Match


# ──────────────────────────────────────────────────────────────────────────────
class Matches(APICall):
    """Entry point for matches
    """

    # ──────────────────────────────────────
    def history(self, **kwargs):
        """Return the requested set of matches

        :param player_name: filter with player name (exact match only)
        :param hero_id: filter with hero ID
        :param game_mode: filter with game mode, 2 for tournament
        :param skill: 0 for any, 1 for normal, 2 for high, 3 for very high skill
        :param date_min: date in UTC seconds since Jan 1, 1970 (unix time format)
        :param date_max: date in UTC seconds since Jan 1, 1970 (unix time format)
        :param min_players: the minimum number of players required in the match
        :param account_id: filter with the given user (32-bit or 64-bit steam ID)
        :param league_id: filter with a league ID
        :param start_at_match_id: start the search at the indicated match ID (descending)
        :param matches_requested: maximum is 25 matches (default is 25)
        :param tournament_games_only: 1 for tournament games only
        :returns: <type 'list'>
        """

        # Pulling datas from Steam
        self.url = 'https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?'
        results = self.get(**kwargs)
        matches = []
        for result in results['result']['matches']:
            matches.append({
                'steam_id': result['match_id'],
                'date':     dt.fromtimestamp(int(result['start_time'])),
            })

        # Return the computed datas
        return matches

    # ──────────────────────────────────────
    def details(self, **kwargs):
        """Return the requested match details

        :param match_id: the ID of the match to fetch
        :returns: <type 'dict'>
        """

        # Pulling datas from Steam
        self.url = 'https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/V001/?'
        results = self.get(**kwargs)
        match = {}
        match['result'] = 'R' if results['result']['radiant_win'] else 'D'
        match['duration'] = results['result']['duration']

        # Pulling specific datas to tournament mode
        try:
            match['league'] = League.objects.get(steam_id=results['result']['leagueid'])
            match['radiant'] = results['result']['radiant_name']
            match['dire'] = results['result']['dire_name']
            pass
        except KeyError as e:
            raise APICallError('KeyError on {}'.format(e.args))

        # Return the computed datas
        return match



# ──────────────────────────────────────────────────────────────────────────────
def fetch_details(**kwargs):
    """Fetch the details for the given match and feed them to the database

    :param date: the date of the match (datetime object)
    :param **kwargs: see Matches.details params
    """

    steam_id = kwargs.pop('steam_id', None)
    date = kwargs.pop('date', None).replace(tzinfo=utc)
    print '  Pulling details for match {}, please wait...'.format(steam_id)
    details = Matches().details(match_id=steam_id, **kwargs)
    details['date'] = date
    match, created = Match.objects.get_or_create(steam_id=steam_id, defaults=details)
    if not created:
        for k,v in details.iteritems():
            setattr(match, k, v)
        match.save()
    print '  Successfully fetched details for match {}.'.format(steam_id)


# ──────────────────────────────────────────────────────────────────────────────
def fetch_history(**kwargs):
    """Fetch the history of the matches in order to feed them to the database

    :param **kwargs: see Matches.history params
    """

    print 'Pulling history of matches, please wait...'
    arguments = {
        'matches_requested':     25,
        'tournament_games_only': 1,
    }
    arguments.update(kwargs)
    matches = Matches().history(**arguments)
    for i,match in enumerate(matches):
        print '  Pulling match {} of {}'.format(i+1, len(matches))
        fetch_details(steam_id=match['steam_id'], date=match['date'])
    print 'Successfully fetched history of {} last matches.'.format(len(matches))
