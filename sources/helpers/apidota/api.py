#! -*- coding: utf-8 -*-
from requests import get
from urllib import urlencode


# ──────────────────────────────────────────────────────────────────────────────
class APICallError(Exception):
    """Generic error to be raised anywhere in the API
    """

    pass


# ──────────────────────────────────────────────────────────────────────────────
class APICall(object):
    """Generic API call
    """

    key = '8783DADD3411016697230B430CACD0E1'  # Steam Web API key
    url = 'You need to overwrite this URL in the methods'

    # ──────────────────────────────────────
    def get(self, **kwargs):
        """Return the API call result

        :param **kwargs: the GET arguments
        :returns: <type 'dict'>
        :raises: <type 'APICallError'>
        """

        kwargs['key'] = self.key
        kwargs['format'] = 'json'
        arguments = urlencode(kwargs)
        response =  get(self.url+arguments)
        if response.status_code == 200:
            return response.json()
        else:
            raise APICallError(str(response.status_code)+': '+type(self).url+arguments)
