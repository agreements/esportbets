#! /usr/bin/python2.7
#! -*- coding: utf-8 -*-

from matches import fetch_history
from leagues import fetch_leagues
from heroes import fetch_heroes
import sys

CALLS = {
    'h': (fetch_heroes,  {}),
    'l': (fetch_leagues, {}),
    'm': (fetch_history, { 'matches_requested':25 }),
}

def fetch(modes=['h', 'l', 'm']):
    for mode in modes:
        arguments = CALLS[mode][1]
        CALLS[mode][0](**arguments)
