#! -*- coding: utf-8 -*-
from api import APICall
from dota.models import Hero


# ──────────────────────────────────────────────────────────────────────────────
class Heroes(APICall):
    """Entry point for heroes
    """

    # ──────────────────────────────────────
    def heroes(self):
        """Return the hero list

        :returns: <type 'dict'>
        """

        # Pulling datas from Steam
        self.url = 'https://api.steampowered.com/IEconDOTA2_570/GetHeroes/v0001/?'
        results = self.get()

        # Reading datas
        heroes = {}
        for result in results['result']['heroes']:
            heroes[result['id']] = ' '.join(result['name'].lower().split('_')[3:])

        # Return the computed datas
        return heroes


# ──────────────────────────────────────────────────────────────────────────────
def fetch_heroes():
    """Fetch the heroes and feed them to the database
    """

    print 'Pulling heroes, please wait...'
    heroes = Heroes().heroes()
    for i,(k,name) in enumerate(heroes.items()):
        hero, created = Hero.objects.get_or_create(steam_id=k, defaults={'name':name, 'vanilla':name})
        if not created:
            hero.vanilla = name
            hero.save()
        print '  {:03d}/{} ({:03d}: {})'.format(i+1, len(heroes), hero.steam_id, hero.vanilla)
    print 'Successfully fetched {} heroes.'.format(len(heroes))
