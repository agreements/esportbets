#! -*- coding: utf-8 -*-
from datetime import datetime as dt
from django.utils.timezone import utc

from api import APICall
from api import APICallError
from dota.models import League


# ──────────────────────────────────────────────────────────────────────────────
class Leagues(APICall):
    """Entry point for leagues
    """

    # ──────────────────────────────────────
    def leagues(self, **kwargs):
        """Return the leagues

        :returns: <type 'list'>
        """

        # Pulling datas from Steam
        self.url = 'https://api.steampowered.com/IDOTA2Match_570/GetLeagueListing/v0001/?'
        results = self.get(**kwargs)
        leagues = {}
        for result in results['result']['leagues']:
            steam_id = result['leagueid']
            leagues[steam_id] = {
                'steam_id':    steam_id,
                'name':        ' '.join(result['name'].split('_')[2:]),
                'description': result['description'],
                'url':         result['tournament_url'],
            }

        # Building an easily readable object (eg. list)
        leagues = [ league for k,league in leagues.iteritems() ]

        # Return the computed datas
        return leagues


# ──────────────────────────────────────────────────────────────────────────────
def fetch_leagues(**kwargs):
    """Fetch the leagues and feed them to the database

    :param **kwargs: see Leagues.leagues params
    """

    print 'Pulling leagues, please wait...'
    leagues = Leagues().leagues()
    for i,details in enumerate(leagues):
        details['name'] = details['name'].replace('_', ' ')
        league, created = League.objects.get_or_create(steam_id=details['steam_id'], defaults=details)
        if not created:
            for k,v in details.iteritems():
                setattr(league, k, v)
            league.save()
        print '  {:03d}/{} ({:05d}: {})'.format(i+1, len(leagues), league.steam_id, league.name)
    print 'Successfully fetched {} leagues.'.format(len(leagues))
