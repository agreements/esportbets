# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

from helpers.baseclasses.basehuman import BaseHuman
from userena.models import UserenaBaseProfile


# ──────────────────────────────────────────────────────────────────────────────
class Profile(BaseHuman, UserenaBaseProfile):
    """Profile model to use to store the user datas
    """

    user    = models.OneToOneField(User, unique=True, related_name='profile')
    credits = models.FloatField(default=100000.0)
    # ──────────────────────────────────────
    def __unicode__(self):
        return unicode(self.user.username)
