from django.conf.urls import patterns, url

from accounts.forms import SubEditProfileForm


urlpatterns = patterns('userena.views',
    url(r'^(?P<username>[\.\w-]+)/edit/$', 'profile_edit', {'edit_profile_form': SubEditProfileForm}, name='userena_profile_edit'),
)
