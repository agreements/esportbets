#! -*- coding: utf-8 -*-
from django.contrib.auth.models import User

from helpers.baseclasses.baseresource import BaseResource
from tastypie.constants import ALL


# ──────────────────────────────────────────────────────────────────────────────
class UserResource(BaseResource):

    # ──────────────────────────────────────
    class Meta:
        queryset      = User.objects.all()
        resource_name = 'users'
        excludes      = [ 'first_name', 'last_name', 'password' ]
        filtering     = {
            'email':    ALL,
            'id':       ALL,
            'username': ALL,
        }
