#! -*- coding: utf-8 -*-
from django import forms

from userena.forms import EditProfileForm


# ──────────────────────────────────────────────────────────────────────────────
class SubEditProfileForm(EditProfileForm):
    """Homemade form for the user to edit his profile
    """

    def __init__(self, *args, **kwargs):
        super(SubEditProfileForm, self).__init__(*args, **kwargs)
        self.fields.pop('first_name')
        self.fields.pop('last_name')
