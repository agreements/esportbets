#! -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.timezone import utc

from dota.models import Bet, Encounter, League


# ──────────────────────────────────────────────────────────────────────────────
@login_required
def index(request):
    '''Render the standard bets page

    :param request: the Django request object
    :returns: <type 'HttpResponse'>
    '''

    # Fetching encounters
    user = request.user
    limit = datetime.now().replace(tzinfo=utc ) + timedelta(minutes=10)
    upcoming = Encounter.objects.filter(date__gt=limit).exclude(bets__user=user.pk).order_by('date')
    pending = Bet.objects.filter(user=user.pk).order_by('date')

    # Computing the odds for each team
    for e in upcoming:
        bets = Bet.objects.filter(encounter=e)
        bets_dire = bets.filter(bet='D')
        e.odds_dire = len(bets_dire) * 100.0 / len(bets)
        e.odds_radiant = 100 - e.odds_dire

    # Rendering the page0
    context = { 'pending':pending, 'upcoming':upcoming }
    response = render(request, 'dota/standard.html', context)
    return response
