#! -*- coding: utf-8 -*-
from django.contrib import admin
from models import Bet, Encounter, Fantasy, Hero, League, Match, Player, Team


# ──────────────────────────────────────────────────────────────────────────────
class BetAdmin(admin.ModelAdmin):
    list_display  = ('__unicode__', 'pk', 'user', 'date', 'encounter',)
    list_filter   = ('date', 'bet',)
    search_fields = ('user__username', 'encounter__dire', 'encounter__radiant', 'encounter__league__name', 'bet',)


# ──────────────────────────────────────────────────────────────────────────────
class EncounterAdmin(admin.ModelAdmin):
    list_display  = ('__unicode__', 'pk', 'dire', 'radiant', 'league', 'date',)
    list_filter   = ('date',)
    search_fields = ('dire', 'radiant', 'league__name',)


# ──────────────────────────────────────────────────────────────────────────────
class FantasyAdmin(admin.ModelAdmin):
    list_display  = ('__unicode__', 'pk', 'user', 'name', 'slug',)
    search_fields = ('user__username', 'name', 'slug', 'players__username',)


# ──────────────────────────────────────────────────────────────────────────────
class HeroAdmin(admin.ModelAdmin):
    list_display  = ('__unicode__', 'pk', 'steam_id', 'vanilla', 'name',)
    search_fields = ('steam_id', 'vanilla', 'name',)


# ──────────────────────────────────────────────────────────────────────────────
class LeagueAdmin(admin.ModelAdmin):
    list_display  = ('__unicode__', 'pk', 'steam_id', 'name', 'description', 'url',)
    search_fields = ('steam_id', 'name', 'description', 'url',)


# ──────────────────────────────────────────────────────────────────────────────
class MatchAdmin(admin.ModelAdmin):
    list_display  = ('__unicode__', 'pk', 'steam_id', 'dire', 'radiant', 'league', 'duration',)
    list_filter   = ('date', 'result',)
    search_fields = ('steam_id', 'dire', 'radiant', 'league__name',)


# ──────────────────────────────────────────────────────────────────────────────
class PlayerAdmin(admin.ModelAdmin):
    list_display  = ('__unicode__', 'pk', 'username', 'team', 'price', 'full_name',)
    list_filter   = ('position', 'birth_date', 'gender',)
    search_fields = ('username', 'team__name', 'price', 'full_name',)


# ──────────────────────────────────────────────────────────────────────────────
class TeamAdmin(admin.ModelAdmin):
    list_display  = ('__unicode__', 'pk', 'name', 'slug',)
    search_fields = ('name', 'slug',)


admin.site.register(Bet, BetAdmin)
admin.site.register(Encounter, EncounterAdmin)
admin.site.register(Fantasy, FantasyAdmin)
admin.site.register(Hero, HeroAdmin)
admin.site.register(League, LeagueAdmin)
admin.site.register(Match, MatchAdmin)
admin.site.register(Player, PlayerAdmin)
admin.site.register(Team, TeamAdmin)
