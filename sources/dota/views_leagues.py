#! -*- coding: utf-8 -*-
from django.views.generic.list import ListView

from dota.models import League


# ──────────────────────────────────────────────────────────────────────────────
class LeaguesListView(ListView):
    '''List the leagues by name
    '''

    model         = League
    template_name = 'dota/leagues.html'
    queryset      = League.objects.order_by('-steam_id')
