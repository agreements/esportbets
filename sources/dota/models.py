#! -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from helpers.baseclasses.basebet import BaseBet
from helpers.baseclasses.baseencounter import BaseEncounter
from helpers.baseclasses.basehuman import GENDERS
from helpers.baseclasses.baseplayer import BasePlayer
from helpers.baseclasses.baseteam import BaseTeam


POSITIONS = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
)

TEAMS = (
    ('D', 'Dire'),
    ('R', 'Radiant'),
)


# ──────────────────────────────────────────────────────────────────────────────
class Hero(models.Model):
    """Stores Dota 2 heroes
    """

    steam_id = models.PositiveIntegerField(unique=True)
    vanilla  = models.CharField(max_length=32, unique=True)
    name     = models.CharField(max_length=32, unique=True, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.name.title())

    class Meta:
        verbose_name_plural = 'heroes'


# ──────────────────────────────────────────────────────────────────────────────
class League(models.Model):
    """Stores Dota 2 leagues
    """

    steam_id    = models.PositiveIntegerField(unique=True)
    name        = models.CharField(max_length=64, unique=True)
    description = models.TextField(max_length=512)
    url         = models.URLField(max_length=256)

    def __unicode__(self):
        return unicode(self.name)


# ──────────────────────────────────────────────────────────────────────────────
class Match(BaseEncounter):
    """Stores Dota 2 matches
    """

    steam_id = models.PositiveIntegerField(unique=True)
    dire     = models.CharField(max_length=32, default='Dire Team')
    radiant  = models.CharField(max_length=32, default='Radiant Team')
    result   = models.CharField(max_length=1, choices=TEAMS, blank=True, null=True)
    duration = models.PositiveSmallIntegerField(blank=True, null=True)

    def __unicode__(self):
        return unicode(self.steam_id)

    class Meta:
        verbose_name_plural = 'matches'


# ──────────────────────────────────────────────────────────────────────────────
class Team(BaseTeam):
    """Stores Dota 2 teams
    """

    name = models.CharField(max_length=32, unique=True)
    slug = models.CharField(max_length=4, unique=True)

    def __unicode__(self):
        return unicode(self.name)


# ──────────────────────────────────────────────────────────────────────────────
class Player(BasePlayer):
    """Stores Dota 2 players
    """

    team     = models.ForeignKey(Team, on_delete=models.SET_NULL, blank=True, null=True)
    position = models.CharField(max_length=1, choices=POSITIONS, default=POSITIONS[0][0])


# ──────────────────────────────────────────────────────────────────────────────
class Fantasy(BaseTeam):
    """Stores Dota 2 fantasy teams
    """

    user        = models.ForeignKey(User, unique=True)
    name        = models.CharField(max_length=32, default=_('My Fantasy Team'))
    slug        = models.CharField(max_length=4, default=_('MFT'))
    players     = models.ManyToManyField(Player, blank=True)
    players.help_text = ''  # The one true way of removing things you didn't add yourself

    def __unicode__(self):
        return unicode(self.user)

    class Meta:
        verbose_name_plural = 'fantasies'


# ──────────────────────────────────────────────────────────────────────────────
class Encounter(BaseEncounter):
    """Store Dota 2 encounters
    """

    dire    = models.ForeignKey(Team, related_name='dire')
    radiant = models.ForeignKey(Team, related_name='radiant')


# ──────────────────────────────────────────────────────────────────────────────
class Bet(BaseBet):
    """Stores Dota 2 standard bets
    """

    encounter = models.ForeignKey(Encounter, related_name='bets')
    bet       = models.CharField(max_length=1, choices=TEAMS)

    def __unicode__(self):
        return unicode('{}'.format(self.encounter))
