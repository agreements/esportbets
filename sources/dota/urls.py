from django.conf.urls import patterns, url

from dota.views_leagues import LeaguesListView
from dota.views_ranking import RankingListView


# Homemade views
urlpatterns = patterns('dota',
    url(r'^fantasy/$',  'views_fantasy.index',  name='dota-fantasy'),
    url(r'^home/$',     'views_home.index',     name='dota-home'),
    url(r'^matches/$',  'views_matches.index',  name='dota-matches'),
    url(r'^standard/$', 'views_standard.index', name='dota-standard'),
)

# Generic views
urlpatterns += patterns('',
    url(r'^leagues/$', LeaguesListView.as_view(), name='dota-leagues'),
    url(r'^ranking/$', RankingListView.as_view(), name='dota-ranking'),
)
