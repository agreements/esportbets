#! -*- coding: utf-8 -*-
from django.shortcuts import render


# ──────────────────────────────────────────────────────────────────────────────
def index(request):
    '''Render the Dota 2 page

    :param request: the Django request object
    :returns: <type 'HttpResponse'>
    '''

    # Rendering the page
    context = {}
    response = render(request, 'dota/home.html', context)
    return response
