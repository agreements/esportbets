#! -*- coding: utf-8 -*-
from datetime import timedelta
from django.shortcuts import render

from dota.models import League, Match


# ──────────────────────────────────────────────────────────────────────────────
def index(request):
    '''Render the history of matches page

    :param request: the Django request object
    :returns: <type 'HttpResponse'>
    '''

    # Fetching matches
    matches = Match.objects.all().order_by('-date')[:25]

    # Formatting time
    for match in matches:
        try:
            match.duration = timedelta(seconds=match.duration)
        except TypeError:
            match.duration = 0

    # Rendering the page
    context = { 'matches':matches }
    response = render(request, 'dota/matches.html', context)
    return response
