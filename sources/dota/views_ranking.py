#! -*- coding: utf-8 -*-
from django.views.generic.list import ListView

from accounts.models import Profile


# ──────────────────────────────────────────────────────────────────────────────
class RankingListView(ListView):
    '''List the users ordered by credits (descending)
    '''

    model         = Profile
    template_name = 'dota/ranking.html'
    queryset      = Profile.objects.order_by('-credits')
