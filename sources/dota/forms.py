#! -*- coding: utf-8 -*-
from django import forms

from dota.models import Fantasy


# ──────────────────────────────────────────────────────────────────────────────
class FantasyForm(forms.ModelForm):
    """Form for the user to edit his Dota fantasy team, inherit from ``models.Fantasy``
    """

    class Meta:
        model = Fantasy
        exclude = ('user', 'players')
