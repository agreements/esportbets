#! -*- coding: utf-8 -*-
from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS

from accounts.api import UserResource
from dota.models import *
from helpers.baseclasses.baseresource import BaseResource


# ──────────────────────────────────────────────────────────────────────────────
class HeroResource(BaseResource):

    class Meta:
        queryset      = Hero.objects.all()
        resource_name = 'heroes'
        excludes      = []
        filtering     = {
            'steam_id': ALL,
            'vanilla':  ALL,
            'name':     ALL,
        }


# ──────────────────────────────────────────────────────────────────────────────
class LeagueResource(BaseResource):

    class Meta:
        queryset      = League.objects.all()
        resource_name = 'leagues'
        excludes      = []
        filtering     = {
            'steam_id':    ALL,
            'name':        ALL,
            'description': ALL,
            'url':         ALL,
        }


# ──────────────────────────────────────────────────────────────────────────────
class MatchResource(BaseResource):

    league = fields.ForeignKey(LeagueResource, 'league', null=True)

    class Meta:
        queryset      = Match.objects.all()
        resource_name = 'matches'
        excludes      = []
        filtering     = {
            'league':   ALL_WITH_RELATIONS,
            'date':     ALL,
            'steam_id': ALL,
            'dire':     ALL,
            'radiant':  ALL,
            'result':   ALL,
            'duration': ALL,
        }


# ──────────────────────────────────────────────────────────────────────────────
class TeamResource(BaseResource):

    class Meta:
        queryset      = Team.objects.all()
        resource_name = 'teams'
        excludes      = []
        filtering     = {
            'name': ALL,
            'slug': ALL,
        }


# ──────────────────────────────────────────────────────────────────────────────
class PlayerResource(BaseResource):

    team = fields.ForeignKey(TeamResource, 'team')

    class Meta:
        queryset      = Player.objects.all()
        resource_name = 'players'
        excludes      = []
        filtering     = {
            'full_name':  ALL,
            'gender':     ALL,
            'birth_date': ALL,
            'username':   ALL,
            'price':      ALL,
            'team':       ALL_WITH_RELATIONS,
            'position':   ALL,
        }


# ──────────────────────────────────────────────────────────────────────────────
class FantasyResource(BaseResource):

    user    = fields.ForeignKey(UserResource, 'user')
    players = fields.ManyToManyField(PlayerResource, 'players')

    class Meta:
        queryset      = Fantasy.objects.all()
        resource_name = 'fantaisies'
        excludes      = []
        filtering     = {
            'user':    ALL_WITH_RELATIONS,
            'name':    ALL,
            'slug':    ALL,
            'players': ALL_WITH_RELATIONS,
        }


# ──────────────────────────────────────────────────────────────────────────────
class EncounterResource(BaseResource):

    league  = fields.ForeignKey(LeagueResource, 'league')
    dire    = fields.ForeignKey(TeamResource, 'team')
    radiant = fields.ForeignKey(TeamResource, 'team')

    class Meta:
        queryset      = Encounter.objects.all()
        resource_name = 'encounters'
        excludes      = []
        filtering     = {
            'league':  ALL_WITH_RELATIONS,
            'date':    ALL,
            'dire':    ALL_WITH_RELATIONS,
            'radiant': ALL_WITH_RELATIONS,
        }


# ──────────────────────────────────────────────────────────────────────────────
class BetResource(BaseResource):

    user      = fields.ForeignKey(UserResource, 'user')
    encounter = fields.ForeignKey(EncounterResource, 'encounter')

    class Meta:
        queryset      = Bet.objects.all()
        resource_name = 'bets'
        excludes      = []
        filtering     = {
            'user':      ALL_WITH_RELATIONS,
            'date':      ALL,
            'encounter': ALL_WITH_RELATIONS,
            'bet':       ALL,
        }
