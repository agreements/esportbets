#! -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseServerError
from django.shortcuts import render

from accounts.models import Profile
from dota.forms import FantasyForm
from dota.models import Fantasy, Player


# ──────────────────────────────────────────────────────────────────────────────
@login_required
def index(request):
    '''Render the fantasy bets page

    :param request: the Django request object
    :returns: <type 'HttpResponse'>, <type 'HttpResponseServerError'>
    '''

    # Fetching the fantasy team if existing
    try:
        fantasy = Fantasy.objects.get(user=request.user.pk)
    except Fantasy.DoesNotExist:
        fantasy = Fantasy(user=request.user).save()
    response = edit(request, fantasy)

    # Rendering the page
    return response


# ──────────────────────────────────────────────────────────────────────────────
def edit(request, fantasy=None):
    '''Edit or create a Fantasy object

    :param request: the Django request object
    :param fantasy: the Fantasy object to edit if provided
    :returns: <type 'HttpResponse'>, <type 'HttpResponseServerError'>
    '''

    # Fetching the user's profile
    try:
        profile = request.user.profile
    except Profile.DoesNotExist:
        return HttpResponseServerError()

    # Fetching the Fantasy form
    form = FantasyForm(instance=fantasy)

    # Fetching the user's fantasy team
    players = [ None ] * 5
    if fantasy:
        for player in fantasy.players.all():
            players[int(player.position)-1] = player

    # Fetching the player pools
    pools = []
    for i in xrange(1, 6):
        pools.append(Player.objects.filter(position=i).order_by('team__slug'))

    # Rendering the page
    context = { 'form':form, 'players':players, 'pools':pools, 'profile':profile }
    response = render(request, 'dota/fantasy.html', context)
    return response
