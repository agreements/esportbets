================
The Dota feature
================

.. automodule:: dota

-----
Forms
-----

.. autoclass:: dota.forms.FantasyForm

------
Models
------

.. automodule:: dota.models
   :members:

-----
Views
-----

.. automodule:: dota.views
   :members:
