===============
The AJAX module
===============

.. automodule:: ajax

-----
Views
-----

""""""""""""
Dota Related
""""""""""""

.. autofunction:: ajax.views_dota.player_buy

.. autofunction:: ajax.views_dota.player_sell

""""""""""""
Form Related
""""""""""""

.. autofunction:: ajax.views_forms.form_validate

.. autofunction:: ajax.views_forms.form_submit
