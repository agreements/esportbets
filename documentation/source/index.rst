.. Esport Bets documentation master file, created by
   sphinx-quickstart on Thu Mar 13 14:32:47 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

======================================
Esport Bets -- Documentation
======================================

Contents:

.. toctree::
   :maxdepth: 2

   introduction
   helpers
   userena
   dota
   starcraft
   ajax


==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
