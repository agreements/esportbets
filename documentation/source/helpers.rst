=======
Helpers
=======

.. automodule:: helpers

----------
Decorators
----------

.. automodule:: helpers.decorators
   :members:

------------
Base Classes
------------

.. autoclass:: helpers.baseclasses.BaseHuman

.. autoclass:: helpers.baseclasses.BaseTeam

.. autoclass:: helpers.baseclasses.BasePlayer

--------------------
Third Party Snippets
--------------------

""""""""""""""""
MultiSelectField
""""""""""""""""

.. autoclass:: helpers.multiselectfield.MultiSelectFormField

.. autoclass:: helpers.multiselectfield.MultiSelectField
